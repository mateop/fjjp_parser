#include "Tokenizer.h"
#include <typeinfo>
#include <iostream>
#include <string>

Tokenizer::Tokenizer(std::string input)
	: input_{input} {
}

Token Tokenizer::next() {

	if(input_.peek() == '#'){
		input_.ignore(2048,'\n');
	}

	while (input_.peek() == ' '){
		input_.get();
	}

	if (input_.peek() == '\t'){
        input_.get();
        return Token{TokenType::Tab, "\t", 0};
    }
    if (input_.peek() == '\n'){
        input_.get();
        return Token{TokenType::NewLine, "\n", 0};
    }
    if (input_.peek() == '|' || input_.peek() == ','){//separators
    	std::ostringstream output;
        output.put(input_.get());
        return Token{TokenType::Symbol, output.str(), 0};
    }
    if (std::isdigit(input_.peek())) {
		// radimo novi string
		std::ostringstream output;

		while (std::isdigit(input_.peek())) {
			// uzmi znak i dodaj u novi string
			output.put(input_.get());
		}
		return Token{TokenType::Number, output.str(), 0};
	}
	// ako smo na kraju ulaza
		// radimo end token
	if (input_.peek() == EOF) {
		return Token{TokenType::End, "", 0};
	}
	//ostalo je string
	std::ostringstream output;
	output.put(input_.get());
	while ((input_.peek() != '\t' 
		&& input_.peek() != ' ' 
		&& input_.peek() != '|' 
		&& input_.peek() != '\n' 
		&& input_.peek() != ',' 
		&& input_.peek() != std::iostream::traits_type::eof())){
		
		output.put(input_.get());
	}
	
	return Token{TokenType::String, output.str(), 0};

}