#include <string>
#include <vector>
#include "Tokenizer.h"
#include "json/json.h"

#ifndef SRC_PARSER_PARSE_TREE_H_
#define SRC_PARSER_PARSE_TREE_H_

class ASTNode {
public:
    virtual Json::Value print() = 0;
    virtual ~ASTNode();
};

class ASTTokenNode : public ASTNode {
public:
    ASTTokenNode(Token t);
    Json::Value print() override;

private:
    Token t_;
};

class ASTListNode : public ASTNode {
public:
    ASTListNode() {

    }
    Json::Value print() override;
    void add_node(ASTTokenNode* node) {
        children_.push_back(node);
    }
    ~ASTListNode();

private:
    std::vector<ASTTokenNode*> children_;
};

class ASTSentenceNode : public ASTNode {
public:
    ASTSentenceNode() {

    }
    Json::Value print() override;
    void add_node(ASTNode* node) // ASTListNode i ASTTokenNode
    {
        children_.push_back(node);
    }
    ~ASTSentenceNode();

private:
    std::vector<ASTNode*> children_;
};

class ASTFile : public ASTNode {
public:
    ASTFile() {

    }
    Json::Value print() override;
    void add_node(ASTSentenceNode* node) {
        children_.push_back(node);
    }

    ~ASTFile();

private:
    std::vector<ASTSentenceNode*> children_;
};

#endif /* SRC_PARSER_PARSE_TREE_H_ */
