#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Tokenizer.h"
#include "Context.h"
#include "Ast.h"

using namespace std;

void parse(Context&, ASTFile&);
void list(Context&, ASTListNode&);
void row(Context&, ASTSentenceNode&);


void parse(Context& c, ASTFile& parent) {
    while (c.is_next(TokenType::Number))
    {
        ASTSentenceNode *n = new ASTSentenceNode();
        row(c, *n);
        parent.add_node(n);
        //skip newlines
        while(c.is_next(TokenType::NewLine)){
        	c.expect(TokenType::NewLine);
        }
    }
}

void list(Context& c, ASTListNode& parent) {
    while (!c.is_next(TokenType::Tab) && !c.is_next(TokenType::NewLine))
    {
        if (c.is_next(TokenType::String))
        {
            parent.add_node(new ASTTokenNode(c.expect(TokenType::String)));
        }
        else if (c.is_next(TokenType::Symbol))
        {
            c.expect(TokenType::Symbol); 
        }
    }
}

void row(Context& c, ASTSentenceNode& parent) {
	
    ASTTokenNode *id = new ASTTokenNode(c.expect(TokenType::Number));
    c.expect(TokenType::Tab);

    //jer je input super dosljedan morao sam dodati metod expectOr
    /*ASTTokenNode *form = new ASTTokenNode(c.expectOr(TokenType::String, TokenType::Symbol));
    c.expect(TokenType::Tab);*/
    ASTListNode *form = new ASTListNode();
    list(c, *form);
    c.expect(TokenType::Tab);

    /*ASTTokenNode  *lemma = new ASTTokenNode(c.expectOr(TokenType::String, TokenType::Symbol));
    c.expect(TokenType::Tab);*/
    ASTListNode *lemma = new ASTListNode();
    list(c, *lemma);
    c.expect(TokenType::Tab);


    ASTTokenNode *cpostag = new ASTTokenNode(c.expect(TokenType::String));
    c.expect(TokenType::Tab);

    ASTListNode *postag = new ASTListNode();
    list(c, *postag);
    c.expect(TokenType::Tab);

    ASTListNode  *feats = new ASTListNode();
    list(c, *feats);
    c.expect(TokenType::Tab);

    ASTTokenNode *head = new ASTTokenNode(c.expectOr(TokenType::Number, TokenType::String));
    c.expect(TokenType::Tab);

    ASTTokenNode *deprel = new ASTTokenNode(c.expect(TokenType::String));
    c.expect(TokenType::Tab);

    ASTTokenNode *phead = new ASTTokenNode(c.expect(TokenType::String));
    c.expect(TokenType::Tab);

    ASTListNode  *pdeprel = new ASTListNode();
    list(c, *pdeprel);
    
    c.expect(TokenType::NewLine);

    parent.add_node(id);
    parent.add_node(form);
    parent.add_node(lemma);
    parent.add_node(cpostag);
    parent.add_node(postag);
    parent.add_node(feats);
    parent.add_node(head);
    parent.add_node(deprel);
    parent.add_node(phead);
    parent.add_node(pdeprel);

}


int main(){

	ifstream inputFile("ftb1u_short.tsv");
	string str;

	inputFile.seekg(0, ios::end);   
	str.reserve(inputFile.tellg());
	inputFile.seekg(0, std::ios::beg);

	str.assign((istreambuf_iterator<char>(inputFile)),
            istreambuf_iterator<char>());

	//tokenize
	Tokenizer tokenizer(str);
	vector<Token> tokens;
	Token currentTk, previousTk;

	while((currentTk = tokenizer.next()).type != TokenType::End){
		if(currentTk.type == TokenType::End){//break if eof
			tokens.push_back(currentTk);
			break;
		}

		/*
		cout << currentTk.text << "\t";
		if (currentTk.type == TokenType::Symbol) cout << "Symbol" << endl;
		else if (currentTk.type == TokenType::String) cout << "string" << endl;
		else if (currentTk.type == TokenType::Number) cout << "Number" << endl;
		else if (currentTk.type == TokenType::Tab) cout << "tab" << endl;
		else if (currentTk.type == TokenType::NewLine) cout << "Newline" << endl;
		else if (currentTk.type == TokenType::End) cout << "end" << endl;
		*/
		tokens.push_back(currentTk);
	}
	tokens.push_back(Token{TokenType::End, "", 0});

	Context cntx{ tokens };
	ASTFile root;



	parse(cntx, root);
	cntx.expect(TokenType::End);
    cout<<root.print();

	return 0;
}