#include "Context.h"
#include <iostream>
#include <string>

Context::Context(std::vector<Token> tokens)
	: token_list_{ tokens }, token_position_{ 0 } {
}

Token Context::expect(const std::string& str) {
	Token t = next();

	if (t.text != str) {
		throw ParseException("Greska, ocekujemo " + str + " dobili smo " + t.text);
	}

	return t;
}

Token Context::expect(TokenType type) {
	Token t = next();

    if (type == TokenType::String && t.type == TokenType::Number)
        type = TokenType::Number;

	if (t.type != type) {
		Token t1 = next();
		Token t2 = next();
		Token t3 = next();
        throw ParseException("Greska, ocekujemo "
        	+ std::to_string((int)type)
        	+ " dobili smo " + t.text + t1.text + t2.text + t3.text
        	+ " type: "
        	+std::to_string((int)t.type));
    }

	return t;
}
Token Context::expectOr(TokenType type1, TokenType type2){
	Token t = next();

    if (type1 == TokenType::String && t.type == TokenType::Number)
        type1 = TokenType::Number;
    if (type2 == TokenType::String && t.type == TokenType::Symbol)
        type2 = TokenType::Symbol;

	if (t.type != type1 && t.type != type2) {
        throw ParseException("Greska, ocekujemo "
        	+ std::to_string((int)type1)
        	+ " or "
        	+ std::to_string((int)type2)
        	+ " dobili smo " + t.text
        	+ " type: "
        	+std::to_string((int)t.type));
    }

	return t;
}

bool Context::is_next(TokenType type) {
	Token t = peek();

    if (type == TokenType::String && t.type == TokenType::Number)
        type = TokenType::Number;

	return t.type == type;
}

Token Context::next() {
	Token t = token_list_[token_position_];

	if (token_position_ < token_list_.size() - 1) {
		token_position_++;
	}

	return t;
}

Token Context::peek() {
	return token_list_[token_position_];
}

ParseException::ParseException(std::string message) : message_{ message } {}

const char* ParseException::what() const throw() {
	return message_.c_str();
}