#include "Ast.h"
#include <iostream>

ASTNode::~ASTNode() {}

ASTTokenNode::ASTTokenNode(Token t) : t_(t) {}

Json::Value ASTTokenNode::print() {
    return Json::Value(t_.text);
}

Json::Value ASTListNode::print() {
    Json::Value list;
    for (ASTTokenNode* child : children_)
    {
        list.append(child->print());
    }
    return list;
}

Json::Value ASTSentenceNode::print() {

    Json::Value row;
    row["ID"] = children_[0]->print();
    row["FORM"] = children_[1]->print();
    row["LEMMA"] = children_[2]->print();
    row["CPOSTAG"] = children_[3]->print();
    row["POSTAG"] = children_[4]->print();
    row["FEATS"] = children_[5]->print();
    row["HEAD"] = children_[6]->print();
    row["DEPREL"] = children_[7]->print();
    row["PHEAD"] = children_[8]->print();
    row["PDEPREL"] = children_[9]->print();
    return row;
}

Json::Value ASTFile::print() {
    Json::Value root;

    for (ASTNode* p : children_) {
        root["rows"].append(p->print());
    }
    return root;
}


ASTFile::~ASTFile() {
    for (ASTSentenceNode* p : children_) {
        delete p;
    }
}

ASTSentenceNode::~ASTSentenceNode() {
    for (ASTNode* p : children_) {
        delete p;
    }
}

ASTListNode::~ASTListNode() {
    for (ASTTokenNode* p : children_) {
        delete p;
    }
}
