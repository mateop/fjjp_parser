#ifndef SRC_TOKENIZER_TOKENIZER_H_
#define SRC_TOKENIZER_TOKENIZER_H_

#include <sstream>
#include <cctype>

enum class TokenType { String, Number, Tab, NewLine, Symbol, End };

struct Token {
	TokenType type;
	std::string text;
	int number;
};

class Tokenizer {
public:
	Tokenizer(std::string input);
	Token next();

private:
	std::istringstream input_;
};

#endif /* SRC_TOKENIZER_TOKENIZER_H_ */