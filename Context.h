#ifndef SRC_CONTEXT_CONTEXT_H_
#define SRC_CONTEXT_CONTEXT_H_
#include "Tokenizer.h"
#include <sstream>
#include <cctype>
#include <vector>


class Context {
public:
	Context(std::vector<Token>);

	Token expect(const std::string&);
	Token expect(TokenType);
	Token expectOr(TokenType, TokenType);
	bool is_next(TokenType);

private:
	Token next();
	Token peek();

	std::vector<Token> token_list_;
	std::size_t token_position_;
};

class ParseException : public std::exception {
public:
	ParseException(std::string);
	virtual const char* what() const throw();

private:
	std::string message_;
};

#endif /* SRC_CONTEXT_CONTEXT_H_ */